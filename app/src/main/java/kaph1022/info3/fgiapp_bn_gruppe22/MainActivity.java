package kaph1022.info3.fgiapp_bn_gruppe22;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.json.JSONArray;

import kaph1022.info3.fgiapp_bn_gruppe22.ui.Impressum.Impressum;
import kaph1022.info3.fgiapp_bn_gruppe22.ui.Mapview.MapViewFragment;
import kaph1022.info3.fgiapp_bn_gruppe22.ui.Search.SearchFragment;

public class MainActivity extends AppCompatActivity {

    private JSONArray details;

    public void setDetails(JSONArray details) {this.details = details; }
    public JSONArray getDetails() { return details; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_welcome);
         new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent WelcomeIntent = new Intent(MainActivity.this, WelcomeActivity.class);
                startActivity(WelcomeIntent);
                finish();
            }
        },4000); */
        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);
    }
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_menu, menu);
        return true;
    }


    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.e("fehler", "impressum noch nicht gedrückt");
        int id = item.getItemId();

        if (id == R.id.impressum){
            Intent intent = new Intent(MainActivity.this, Impressum.class);
            startActivity(intent);
            Log.e("fehler", "impressum gedrückt");
            return false;
        }
        return super.onOptionsItemSelected(item);
    }


}
