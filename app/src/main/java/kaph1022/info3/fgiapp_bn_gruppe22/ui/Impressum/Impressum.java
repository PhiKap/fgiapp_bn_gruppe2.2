package kaph1022.info3.fgiapp_bn_gruppe22.ui.Impressum;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import kaph1022.info3.fgiapp_bn_gruppe22.R;

public class Impressum extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_impressum);

        TextView Inhalt = (TextView) findViewById(R.id.textView_Impressum_Inhalt);
        Button Kontakt = (Button) findViewById(R.id.button_Impressum_Kontakt);
        final String Mailadresse = "kaph1022@hs-karlsruhe.de";

        Inhalt.setText("Philipp Kappler, kaph1022@hs-karlsruhe.de\n" +
                "Till Epple, epti1012@hs-karlsruhe.de\n" +
                "Maria Hohenöcker, glma1025@hs-karlsruhe.de\n" +
                "Tamara Senst, seta1011@hs-karlsruhe.de\n\n\n" +
                "Hoffstraße 3\n" +
                "76133 Karlsruhe\n\n\n" +
                "Kontaktadresse: kaph1022@hs-karlsruhe.de");


        Kontakt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",Mailadresse, null));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, Mailadresse);

                startActivity(Intent.createChooser(emailIntent, "Kontakt an Entwickler"));
            }
        });
    }
}
