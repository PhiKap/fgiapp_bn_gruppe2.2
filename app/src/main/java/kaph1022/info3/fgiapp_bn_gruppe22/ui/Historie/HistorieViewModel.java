package kaph1022.info3.fgiapp_bn_gruppe22.ui.Historie;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HistorieViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public HistorieViewModel() {
        mText = new MutableLiveData<>();

    }

    public LiveData<String> getText() {
        return mText;
    }
}