package kaph1022.info3.fgiapp_bn_gruppe22.ui.Historie;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import java.util.ArrayList;

import kaph1022.info3.fgiapp_bn_gruppe22.R;
import kaph1022.info3.fgiapp_bn_gruppe22.SuchDatenbank;
import kaph1022.info3.fgiapp_bn_gruppe22.ui.Search.SearchFragment;

public class HistorieFragment extends Fragment {



    private HistorieViewModel historieViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_historie, container, false);


        final TextView VerlaufAnzeige = (TextView) root.findViewById(R.id.textView_Sucherverlauf_Inhalt);
        Button Löschen = (Button) root.findViewById(R.id.button_historie_leeren);
        Button FavLöschen = (Button) root.findViewById(R.id.button_Favoriten_loeschen);
        final TextView FavAnzeige = (TextView) root.findViewById(R.id.textView_Favoriten_Inhalt);

        VerlaufAnzeige.setMovementMethod(new ScrollingMovementMethod());
        FavAnzeige.setMovementMethod(new ScrollingMovementMethod());

        String[] projection = {
            SuchDatenbank.COLUMN_START,
            SuchDatenbank.COLUMN_ZIEL
        };

        String sortOrder = SuchDatenbank.COLUMN_ID + " DESC";
        String selection = SuchDatenbank.COLUMN_FAV + " LIKE ?";

        SuchDatenbank suchDatenbank = new SuchDatenbank((getContext()));
        SQLiteDatabase readable = suchDatenbank.getReadableDatabase();
        String[] FAVargs = {"1"};
        String[] sucheArgs = {"0"};

        Cursor sucheCursor =  readable.query(
                SuchDatenbank.TABLE_SUCHLOG,
                projection,
                selection,
                sucheArgs,
                null,
                null,
                sortOrder,
                "10"
        );
        Cursor FAVcursor =  readable.query(
                SuchDatenbank.TABLE_SUCHLOG,
                projection,
                selection,
                FAVargs,
                null,
                null,
                sortOrder,
                "10"
        );


        String Favoritenliste = "";
        String Gesamtverlauf = "";

        while (sucheCursor.moveToNext()) {
            String von = sucheCursor.getString(0);
            String nach = sucheCursor.getString(1);
            Gesamtverlauf += "Von: "+ von + "\n"
                    + "Nach: " + nach + "\n\n";
        }
        VerlaufAnzeige.setText(Gesamtverlauf);

        while (FAVcursor.moveToNext()) {
            String von = FAVcursor.getString(0);
            String nach = FAVcursor.getString(1);
            Favoritenliste += "Von: "+ von + "\n"
                    + "Nach: " + nach + "\n\n";
        }
        FavAnzeige.setText(Favoritenliste);


        Löschen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SuchDatenbank suchDatenbank = new SuchDatenbank((getContext()));
                SQLiteDatabase writable = suchDatenbank.getWritableDatabase();
                writable.delete(SuchDatenbank.TABLE_SUCHLOG, SuchDatenbank.COLUMN_FAV + " LIKE 0", null);

                VerlaufAnzeige.setText("");
            }
        });

        FavLöschen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SuchDatenbank suchDatenbank = new SuchDatenbank((getContext()));
                SQLiteDatabase writable = suchDatenbank.getWritableDatabase();
                writable.delete(SuchDatenbank.TABLE_SUCHLOG, SuchDatenbank.COLUMN_FAV + " LIKE 1", null);

                FavAnzeige.setText("");
            }
        });

        return root;


    }

}