package kaph1022.info3.fgiapp_bn_gruppe22.ui.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import kaph1022.info3.fgiapp_bn_gruppe22.R;

public class FahrtenListAdapter extends ArrayAdapter<Fahrten> {

    private static final String TAG = "FahrtenListAdapter";

    private Context mContext;


    public FahrtenListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Fahrten> objects) {
        super(context, resource, objects);
        mContext = context;


    }

    public View getView(int position, View convertView, ViewGroup parent){
        DateFormat dateformat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        String Starthaltestelle = getItem(position).getStarthalte();
        String StartUhrzeit = getItem(position).getStartUhrzeit();
        String Verkehrsmittel = getItem(position).getVerkehrsmittel();
        String Endhaltestelle = getItem(position).getEndhalte();
        String EndUhrzeit = getItem(position).getEndUhrzeit();


        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.adapter_list_view, parent, false);


        TextView tvSH = (TextView) convertView.findViewById(R.id.TextView_StartHalte);
        TextView tvSU = (TextView) convertView.findViewById(R.id.TextView_StartZeit);
        TextView tvVM = (TextView) convertView.findViewById(R.id.textView_Verkehrsmittel);
        TextView tvEH = (TextView) convertView.findViewById(R.id.textView_Endhalte);
        TextView tvEZ = (TextView) convertView.findViewById(R.id.textView_Endzeit);

        tvSH.setText(Starthaltestelle);
        tvSU.setText(dateformat.format(Date.from(Instant.parse(StartUhrzeit))));
        tvVM.setText(Verkehrsmittel);
        tvEH.setText(Endhaltestelle);
        tvEZ.setText(dateformat.format(Date.from(Instant.parse(EndUhrzeit))));

        return convertView;
    }
}
