package kaph1022.info3.fgiapp_bn_gruppe22;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;


public class SuchDatenbank extends SQLiteOpenHelper {
    //database
    public static final String DB_NAME = "suche.db";
    public static final int DB_VERSION = 1;

    //table
    public static final String TABLE_SUCHLOG = "suchLog";
    public static final String COLUMN_ID = "_ID";
    public static final String COLUMN_START = "start";
    public static final String COLUMN_ZIEL = "ziel";
    public static final String COLUMN_FAV = "fav";

    private final String SQL_CREATE =
            "create table " + TABLE_SUCHLOG + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_START + " text not null, " +
                    COLUMN_ZIEL + " text not null, " +
                    COLUMN_FAV + " boolean not null);";

    private final String SQL_DROP = "drop table if exists " + TABLE_SUCHLOG;

    public SuchDatenbank(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        emptyTable(db);
    }

    public void emptyTable(SQLiteDatabase db) {
        db.execSQL(SQL_DROP);
        onCreate(db);
    }
}
